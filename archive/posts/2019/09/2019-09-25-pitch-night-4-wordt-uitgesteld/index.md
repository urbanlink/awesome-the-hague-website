---
title: "Pitch night 4 wordt uitgesteld"
date: "2019-09-25"
---

Helaas, Pitch Night #4 die we hadden gepland op 25 september 2019 moeten we helaas uitstellen. We hebben hard gewerkt om voldoende Trustees en Inzenders te mobiliseren, maar helaas is dat nog niet helemaal gelukt.

We zijn wel op de goede weg, dus het is uitstel, zeker geen afstel. Hou de site in de gaten, want we laten het meteen weten wanneer de nieuwe pitch night plaatsvind.

We zijn dus nog hard op zoek naar nieuwe Trustees. Ben jij er 1, of weet je er 1 (of meer :) ), laat het weten!

Tot de volgende Pitch Night!
