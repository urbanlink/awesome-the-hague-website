---
title: "Anjel versterkt Awesome The Hague"
date: "2019-10-08"
---

**Anjel Punte,**

is als relatieve nieuwkomer in Den Haag de stad aan het ontdekken. Stuitend op de vele leuke gezichten die onze stad kent, begon het bij haar te kriebelen om die bij elkaar te brengen. Met een achtergrond in journalistiek en politiek heeft zij ervaring in het opduikelen van opmerkelijke verhalen en perspectieven. 

![](images/Anjel-Portret-768x1024.jpg)

> "_Ik heb zin om met jou en de rest van het Awesome The Hague-team de volgende Pitch Night tot een succes te maken. Met verrassende ideeën voor Den Haag, en een avond vol mensen waarvan je nu nog niet weet dat je ze wil leren kennen._"
> 
> Anjel

Vanaf nu gaat Anjel op zoek naar spraakmakende gezichten met nieuwe inzichten. Én naar de trustees die de beste ideeën mede mogelijk willen maken!
