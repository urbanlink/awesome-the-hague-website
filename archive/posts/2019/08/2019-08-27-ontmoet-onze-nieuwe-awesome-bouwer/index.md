---
title: "Ontmoet onze nieuwe Awesome bouwer!"
date: "2019-08-27"
---

**Nout Hogesteeger**,

is op meerdere plekken in den haag actief om mensen en initiatieven bij elkaar te brengen. Als product ontwerper heeft hij ervaring op gedaan met het oplossen van problemen samen met en voor verschillende doelgroepen. Nu is past hij die ervaring toe om zo pragmatisch mogelijk van idee naar uitvoering te komen.

![](images/zelf_oscars_foto-2-787x1024.jpg)

> "_Ik kom bij Awesome Den Haag het team versterken omdat ik geloof dat initiatieven die direct van inwoners en gebruikers van de stad komen het beste werken. Hoe minder stappen er tussen de mensen, de ideeën en de uitvoer zitten, hoe beter_"
> 
> Nout

Voor de komende tijd gaat Nout focussen op community building, zowel om onze supporters poel nog verder uit te bereiden en door mooie initiatieven en ideeën te zoeken die we samen supporters kunnen uitnodigen voor het volgende Pitch event, mis m niet ;)

Let's go!
