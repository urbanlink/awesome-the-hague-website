---
title: "Aankondiging: Awesome Pitch Night #4"
date: "2019-08-27"
---

We gaan weer van start met Awesome The Hague. De pitch night voor waanzinnige ideeën voor een meer awesome Den Haag. 20 mensen (de Trustees) leggen gezamenlijk €1.000,- in de pot en geven dit aan het idee dat zij het meest awesome vinden. Deze winnaar gaat met de pot naar huis, '_No strings attached_'!

Samen met Het Koorenhuis organiseren we iedere 2 maanden een Awesome The Hague Pitch Night.

Editie nummer 4 staat gepland op woensdag 25 september om 19 uur in de foyer van het Koorenhuis. Om van Awesome The Hague een succes te maken zijn 20 Trustees nodig, en 5 waanzinnige ideeën. Wil je meedoen? Er is nu nog ruimte om mee te doen als Trustee (je legt 50€ in de pot en mag stemmen) of als inzender van een awesome idee.

Meld je dus nu aan als [Trustee](https://awesomethehague.org/trustee-aanmelding/) of als [Inzender](https://awesomethehague.org/pitch-nights/). Gewoon gezellig langskomen kan natuurlijk ook, reserveer hier je ticket.

Bekijk voor de laatste details over de komende Pitch Night op de [event pagina](https://awesomethehague.org/pitch-nights/)!

**Let's make The Hague More Awesome!**
