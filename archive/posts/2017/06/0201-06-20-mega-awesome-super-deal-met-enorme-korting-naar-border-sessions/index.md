---
title: "Mega AWESOME super deal: Met enorme korting naar Border Sessions"
date: "0201-06-20"
coverImage: "awesome_ticket_bordersessions.png"
---

Donderdag 29 juni om 17 uur is [Awesome Pitch Night #3](https://www.haagsemakers.nl/agenda/awesome-the-hague-pitch-night-3-2/), tijdens [Border Sessions](http://bordersessions.org). Border Sessions is dé jaarlijkse internationale technologie conferentie in Den Haag waar iedere maker bij aanwezig zou moeten zijn!

**En dat is nu mogelijk.** We hebben een mega AWESOME super deal weten af te spreken met Border Sessions. Voor maar **€15,-** kun je via Haagse Makers een AWESOME Day Ticket bestellen! De normale prijs voor zo'n ticket is €75,- (YEAH)! We nodigen je uit om samen met ons het Border Sessions festival te ontdekken, om een dag technologen, onderzoekers, artiesten en activisten te horen vertellen over hun projecten, missies en ontdekkingen.

Waarom? Omdat ook Border Sessions wil bijdragen aan een meer [Awesome Den Haag](http://awesome.haagsemakers.nl)!

En dat is nog niet alles. De €15,- van het ticket wordt door Border Sessions volledig gedoneerd aan [Precious Plastic Den Haag](https://www.haagsemakers.nl/project/replastic/)! Dus hoe meer awesome makers komen, hoe meer Awesome Precious Plastic wordt! Precious Plastic was de winnaar van AWESOME The Hague #2. Met iedere bijdrage kunnen de machines om lokaal plastic te recyclen worden uitgebreid.

Kom naar Border Sessions, en kom naar de AWESOME Pitch Night #3 op donderdag 29 juni om 17:00 uur!

**[Bestel je AWESOME Day Ticket hier](https://www.haagsemakers.nl/agenda/awesome-the-hague-pitch-night-3-2/)**
