---
title: "In 2017 wordt Den Haag AWESOME!"
date: "2017-01-24"
---

## **Aankondiging: We starten AWESOME Den Haag in 2017!**

Den Haag wordt in 2017 nóg meer AWESOME! Met Haagse Makers werken we aan een platform voor vindingrijk Den Haag. We willen ervoor zorgen dat iedereen die iets wil maken, iets bedacht heeft of met een plannetje rondloopt de mogelijkheden heeft om dit in de stad uit te voeren.

Eén van de manieren hoe we dit willen bereiken is met **AWESOME Den Haag**. Wat is AWESOME? [Een crowdfunding avond voor waanzinnige nieuwe en AWESOME ideeën voor onze stad](https://haagsemakers.nl/?page_id=8348). Dit gaan we iedere maand doen, op de laatste donderdag van de maand. Met AWESOME Den Haag willen we bereiken dat iedereen die Den Haag nog leuker, aantrekkelijker, open en positief wil maken (kortom AWESOME), deze plannen tijdens AWESOME Den Haag kan laten zien.

## Hoe werkt Awesome Den Haag?

Het is een crowd-funding avond, dus het publiek draagt zorg voor de funding. Voor 10€ krijg je een stembiljet en kun je stemmen op het beste idee van de avond. Het idee met de meeste stemmen gaat met de pot naar huis (1000€). AWESOME Den Haag is een mooi moment om je idee te laten zien, nieuwe mensen te leren kennen en het netwerk van vindingrijk Den Haag verder te versterken. Dit kun je doen door jouw eigen idee voor een Awesome Den Haag aan te melden, door een ticket te kopen (als persoon of als organisatie), of door partner te worden.

We hebben een te gekke locatie voor Awesome Den Haag: [De Bazaar of Ideas](http://www.bazaarofideas.com/), vlakbij station HS. Een mooie plek om te gekken nieuwe ideeën voor de stad met elkaar te delen.

Enthousiast? Kijk op [https://haagsemakers.nl/awesome](https://haagsemakers.nl/awesome) voor meer info!

## De eerste stap? Samenwerking!

Om AWESOME Den Haag te kunnen organiseren kunnen we jouw hulp goed gebruiken. We starten AWESOME op eigen initiatief, dus zonder financiering. Om een mooie avond te kunnen organiseren, goed te kunnen communiceren en alles te regelen zijn we op zoek naar partners. We willen met zoveel mogelijk partijen in de stad samenwerken, dus we hebben het makkelijk gemaakt. Er zijn 4 mogelijkheden om AWESOME Den Haag mogelijk te maken, voor €10, €100, €1000 en €10.000. Kijk op [https://haagsemakers.nl/awesome](https://haagsemakers.nl/awesome) voor meer informatie.

> Onze eerste target? Wie zijn de 100 meest AWESOME organisaties van Den Haag? Wordt supporter voor €100,- of partner voor €1000,- met jouw organisatie en laat zien dat jij Den Haag ook meer AWESOME wilt maken!

Doe mee, samen maken we Den Haag nog meer AWESOME!
