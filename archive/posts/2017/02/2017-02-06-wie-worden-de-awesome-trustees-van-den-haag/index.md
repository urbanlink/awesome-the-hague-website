---
title: "Wie worden de AWESOME Trustees van Den Haag?"
date: "2017-02-06"
coverImage: "awesome_trustee_header-1.png"
---

Samen maken we Den Haag meer AWESOME! In maart starten we met Awesome Den Haag, een crowdfunding avond voor waanzinnige nieuwe plannen in onze stad.

Om te kunnen starten zoeken we 20 mensen die Trustee worden van Awesome Den Haag. Als trustee ben je de basis voor de Awesome Den Haag crowdfunding avond. De 20 trustees zorgen ervoor dat Awesome Den Haag kan plaatsvinden.

Dus, draag jij Den Haag een warm hart toe en wil je dat we samen Den Haag (nog) meer AWESOME maken? Wordt dan Trustee!

![](images/Awesome-Rotterdam-1024x536.jpg)

## Wat moet ik doen als trustee?

Als trustee doneer je iedere editie €50,-. Dit bedrag gaat rechtstreeks naar de winnaar van de avond. Als trustee (donateur) mag je stemmen op de ideeën die die avond zijn gepresenteerd.

Daarnaast ben je natuurlijk ook aanwezig op de avond zelf. En we vragen je om actief het AWESOME concept te verspreiden in de stad.

Samen maken we van Awesome Den Haag een succes!

Trustee worden? [Neem nu contact op](/?page_id=42)! Er zijn een beperkt aantal plaatsen beschikbaar.

 

Meer weten over Awesome Den Haag? Kijk op [https://haagsemakers.nl/awesome](https://haagsemakers.nl/awesome) of op [http://www.awesomefoundation.org](http://www.awesomefoundation.org)
