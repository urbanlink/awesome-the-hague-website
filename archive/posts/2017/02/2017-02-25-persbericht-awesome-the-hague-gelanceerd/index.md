---
title: "Persbericht: Awesome The Hague gelanceerd"
date: "2017-02-25"
coverImage: "awesome-header.png"
---

## Persbericht

In navolging van 85 steden in 18 verschillende langen wordt in Den Haag een 'chapter' van de Awesome Foundation opgezet. Met dit nieuwe initiatief krijgen kleurrijke, innovatieve en creatieve mensen uit Den Haag binnenkort de kans om hun wildste ideeën te realiseren. Ideeën die Den Haag 'even more awesome' maken. Twintig Haagse 'trustees' doneren iedere maand 50 euro en bepalen samen welk awesome idee de duizend euro verdient.

Haagse Makers haalt de Awesome Foundation naar Den Haag. De Awesome Foundation is een internationaal netwerk van autonome groepen trustees die elke maand geld ter beschikking stellen om een gaaf idee te realiseren. De Haagse chapter wordt een diverse groep van twintig trustees die allen een sterke band hebben met Den Haag. Zij zullen één keer per maand bij elkaar komen op de zogeheten 'Pitch Night'. Daar bepalen ze welk idee Den Haag 'even more awesome' maakt. De trustees stellen tevens hun netwerk en ervaring ter beschikking om het idee succesvol te lanceren.

Er bestaan vele initiatieven om innovatieve en creatieve ideeën een financiële boost te geven. Veel van deze ideeën kunnen echter niet worden uitgevoerd door financiële en administratieve drempels. Awesome The Hague wil deze drempel wegnemen. "Elke maand ontvangt één idee 1.000 euro. Het enige wat we van de winnaar van het idee verwachten, is het uitvoeren van het idee. Op deze manier zorgen wij ervoor dat gedurfde ideeën die bijvoorbeeld in een bar ontstaan niet stranden, maar daadwerkelijk worden uitgevoerd. De eerste Awesome Foundation is in 2009 in Boston uitgevoerd. In Boston kwam iemand met het idee om 's werelds grootste hangmat te maken, in Los Angeles werd het geld gebruikt om door de hele stad schommels op te hangen, ook werd hier 1000 dollar ingezet tegen laaggeletterdheid.

## Awesome The Hague Pitch Night

Op de website [https://haagsemakers.nl/awesome](https://haagsemakers.nl/awesome) kunnen geïnteresseerden die trustee willen worden zich aanmelden. De eerste aanmeldingen zijn al binnen, dus we raden aan niet te lang te wachten. Ook kunnen via de website ideeën worden ingestuurd. Op de website is meer informatie te vinden over de eerste Pitch Night op 23 maart 2017 en de uiterste datum om jouw awesome idee in te sturen.

De Awesome The Hague Pitch Nights vinden op de derde donderdag van de maand plaats in de Bazaar of Ideas, Hoefkade 11 Den Haag.

### Voor meer informatie:

Arn van der Pluijm [awesome@haagsemakers.nl](mailto:awesome@haagsemakers.nl) +31 6 1809 2978
