---
title: "Awesome Den Haag: 10 out of 20 Trustees reached!"
date: "2017-02-15"
coverImage: "awesome_10_trustee_available.png"
---

We are halfway with the 20 Trustees for Awesome Den Haag! The 20 trustees make the €1000,- no-strings-attached micro-grant possible. Together we create an even more awesome The Hague!

We are now halfway, there are still 10 seats available to become a trustee. As a trustee, you make Awesome the Hague possible. You are the foundation of the Pitch Events, the network of awesome people and organisations in the city and to create more awesomeness in our city.

As Trustee, you have 1 vote to decide what is the best, unusual and local project for The Hague. Each edition, you make a donation of €50,-. Combined with the other trustees, this makes the €1000,- grant for the winner!

Ready to become a trustee? [Take a look at the awesome page](/?page_id=8348), [or contact us now](/?page_id=42)!

 

**Let's make The Hague even more awesome!**
