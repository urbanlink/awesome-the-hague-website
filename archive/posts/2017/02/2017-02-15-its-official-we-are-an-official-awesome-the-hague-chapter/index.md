---
title: "Its Official, we are an official Awesome The Hague Chapter!"
date: "2017-02-15"
coverImage: "awesome_official_chapter@2x.png"
---

Yes, we are now an official chapter of the [Awesome Foundation](http://awesomefoundation.org)! This means we are part of a worldwide movement of people providing micro-grants to people with awesome ideas!

Did you now that there are currently 2496 projects funded by the Awesome Foundation, summing up to a total of $2.496.000! Let's contribute to the great development in The Hague as well!

**First step**: We need to gather 20 Trustees who chip in 50€ per person. This money will be awarded unconditionally to the winner of the Awesome Pitch Night.

The first Awesome night will be held at Thursday March 23 in the Bazaar of Ideas. We will keep you posted about further developments!
